<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 	
		<title>ユーザー編集</title>
		<link href="./css/edit.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="title"><h1>ユーザー編集</h1></div>

        
		<div class="header">
			<div class="left-header">
				<div class="headHome"><a href="./" class="home">ホーム</a></div>
	    		<div class="headUserManagement"><a href="userManagement" class="userManagement">ユーザー管理</a></div>
      			<div class="headSignup"><a href="signup" class="signup">ユーザー登録</a></div>
			</div>
			<div class="right-header">
				<div class="profile"><c:out value="ログイン中：${loginUser.name}さん　" />
				<div class="logout"><a href="logout" class="logout">ログアウト</a></div></div>		
			</div>
		</div>        
 
        <div class="errorMessages">
       		<c:if test="${ not empty errorMessages }">
	            <c:forEach items="${errorMessages}" var="loginUser">
	                <li><c:out value="${loginUser}" />
	            </c:forEach>    
            <c:remove var="errorMessages" scope="session" />
            </c:if>
		</div>
        <div class="main-contents">
            <form action="edit" method="post">
            <ul>
             	<li class="name">
                <label for="name">名前(10文字以下)</label><br> 
                <input name="name" value="${editUser.name}" id="name" />
 				</li>
				<li class="loginId">
                <label for="loginId">ログインID(半角英数6文字以上20文字以下)</label><br> 
                <input name="loginId"  value="${editUser.loginId}" id="loginId" />
                <input type="hidden" name="userId" value="${editUser.id}" id="userId">
 				</li>
 				<li class="password">
                <label for="password">パスワード(半角英数記号6文字以上20文字以下)</label><br> 
                <input name="password" type="password" id="password" />
 				</li>
 				<li class="passwordCheck">
                <label for="passwordCheck">パスワード確認</label><br> 
                <input name="passwordCheck" type="password" id="passwordCheck" /> 
 				</li>
				<c:if test="${editUser.id == loginUser.id}">
					<br />支店： 本店<br /><br />部署・役職： 総務人事
					<input type="hidden" name="branchId" value="1" id="branchId">
					<input type="hidden" name="positionId" value="1" id="positionId">
				</c:if>
				<c:if test="${editUser.id != loginUser.id}">
				<li class="branchId">
					<label for="branchId">支店</label><br>
					<select name="branchId">
						<c:forEach items="${branches}" var="branch">
							<c:if test="${branch.id == editUser.branchId}">
								<option value="${branch.id}" id="branchId" selected> ${branch.name}</option>
							</c:if>
							<c:if test="${branch.id != editUser.branchId}">
								<option value="${branch.id}" id="branchId"> ${branch.name}</option>
							</c:if>
						</c:forEach>
					</select>
				</li>
				<li class="positionId">
					<label for="positionId">部署・役職</label><br>
					<select name="positionId">
						<c:forEach items="${positions}" var="position">
							<c:if test="${position.id == editUser.positionId}">
								<option value="${position.id}" id="positionId" selected> ${position.name}</option>
							</c:if>
							<c:if test="${position.id != editUser.positionId}">
								<option value="${position.id}" id="positionId"> ${position.name}</option>
							</c:if>
						</c:forEach>
					</select>
				</li>
				</c:if>
	        	<%--活動状態：
	        	<c:if test="${editUser.isStopped == 0}">活動中</c:if>
	        	<c:if test="${editUser.isStopped == 1}">停止中</c:if>--%>
				<li class="submit"><br><input id="submit" type="submit" value="編集" />
				</li>
			</ul>
            </form>
        </div>
        <div class="copyright">Copyright(c)Ichikawa</div>
	</body>
</html>

