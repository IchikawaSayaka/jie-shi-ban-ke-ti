<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- HTMLの型を宣言 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム</title>
		<link href="./css/top.css" rel="stylesheet" type="text/css">
		<script>		
			function messagedelete(title) {
			    if (window.confirm(title + 'の投稿を削除しますか？')) {
			        return true;
				} else {
					return false;
			    }
			}
		</script>
		<script>
			function commentdelete(title) {
			    if (window.confirm(title + 'へのコメントを削除しますか？')) {
			        return true;
				} else {
					return false;
			    }
			}
		</script>
		
	</head>
	<body>
		<!-- div class で複数文をひとまとまりにする -->
		<div class="title"><h1>社内掲示板</h1></div>
		<div class="header">
			<div class="left-header">
				<div class="headNewMessage"><a href="newMessage" class="newMessage">新規投稿</a></div>
				<div class="headUserManagement"><c:if test="${loginUser.positionId == 1}" ><a href="userManagement" class="userManagement">ユーザー管理</a></c:if></div>
			</div>
			<div class="right-header">
				<div class="profile"><c:out value="ログイン中：${loginUser.name}さん　" />
				<div class="logout"><a href="logout" class="logout">ログアウト</a></div></div>		
			</div>
		</div>
		
		<div class="search-area">
			<h2>投稿検索</h2>
			<div class="search">
				<form action="./" method="get">
		       		<label for="search">カテゴリー <input name="searchCategory" value="${searchCategory}" id="searchCategory" /></label>
		       		<label for="firstDate">　投稿日 <input type="date" name="firstDate" value="${firstDate}" id="firstDate" /></label>
		       		<label for="lastDate"> ～ <input type="date" name="lastDate" value="${lastDate}" id="lastDate" /></label>
					<input id="searchButton" type="submit" value="検索">
				</form>
				<form action="./" method="get">
					<input type="hidden" name="searchCategory" value="">
					<input type="hidden" name="firstDate" value="">
					<input type="hidden" name="lastDate" value="">
					<input id="reset" type="submit" value="リセット">
				</form>
				<c:remove var="searchCategory" scope="session" />
				<c:remove var="firstDate" scope="session" />
				<c:remove var="lastDate" scope="session" />
			</div>
        </div>
        
		<div class="errorMessages">			
            <c:if test="${ not empty managementErrorMessages }">
                <ul>
                    <c:forEach items="${managementErrorMessages}" var="message">
                        <li><c:out value="${message}" />
                    </c:forEach>
                </ul>
                <c:remove var="managementErrorMessages" scope="session"/>
            </c:if>
            <c:if test="${ not empty errorMessages }">
         		<ul>
			 　 	<c:forEach items="${errorMessages}" var="comments">
                      	   <li><c:out value="${comments}" />
                   	</c:forEach>
				</ul>
        		<c:remove var="errorMessages" scope="session" />
        	</c:if> 
        </div>
		<div class="main-contents">	
			<c:if test="${ not empty loginUser }">
	    		<div class="messages">
	    		<h2>投稿一覧</h2>
					<c:forEach items="${messages}" var="message">
						<div class="message">
							<div class="title">　<c:out value="${message.title}" /></div>
							<div class="category">　カテゴリー：　<c:out value="${message.category}" /></div>
							<div class="name">　投稿者：　<c:out value="${message.name}" />　　　　投稿日時:　<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
							<div class="text"><br /><c:forEach var="text" items="${fn:split(message.text,' 
							')}">${text}<br></c:forEach><br /></div>
						</div>
						<div class="message.delete">
							<div class="comment.delete">
								<c:if test = "${message.name == loginUser.name || loginUser.positionId == 2}">
									<form action="messageDelete" method="post">
									<input type="hidden" name="messageId" value="${message.id}" id="messageId">
									<input id="delete" type="submit" onClick="return messagedelete('${message.title}')" value="投稿削除">
									</form>
								</c:if>
								<c:if test = "${message.name != loginUser.name && loginUser.positionId == 3 && loginUser.branchId == message.positionId}">
									<form action="messageDelete" method="post">
									<input type="hidden" name="messageId" value="${message.id}" id="messageId">
									<input id="delete" type="submit" onClick="return messagedelete('${message.title}')" value="投稿削除">
									</form>
								</c:if>
							</div>
						</div>
						<div class="comment">
							<c:forEach items="${comments}" var="comment">
								<%--${message.id}は、var.beansでsetしたもの --%>
								<c:if test = "${message.id == comment.messageId}">
									<div class="name">　投稿者：　<c:out value="${comment.name}" />　　　　投稿日時：　<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
									<div class="text"><br /><c:forEach var="text" items="${fn:split(comment.text,' 
									')}">${text}<br></c:forEach><br /></div>
									<div class="comment.delete">
										<c:if test = "${comment.name == loginUser.name || loginUser.positionId == 2}">
										<form action="commentDelete" method="post">
											<input type="hidden" name="commentId" value="${comment.id}" id="commentId">
											<input id="delete" type="submit" onClick="return commentdelete('${message.title}')" value="コメント削除">
										</form>
										</c:if>
										<c:if test = "${comment.name != loginUser.name && loginUser.positionId == 3 && loginUser.branchId == comment.positionId}">
										<form action="commentDelete" method="post">
											<input type="hidden" name="commentId" value="${comment.id}" id="commentId">
											<input id="delete" type="submit" onClick="return commentdelete('${message.title}')" value="コメント削除">
										</form>
										</c:if>
									</div>
								</c:if>
							</c:forEach>
							<div class="form-area">
	        					<form action="newComment" method="post">
	        						<input type="hidden" name="messageId" value="${message.id}" id="messageId">
			          				コメント（500文字以下）<br />
			          				<%-- ${message.id}/${reComment.messageId}--%>
			           					<textarea name="text" cols="110" rows="6" class="tweet-box"><c:if test = "${message.id == reComment.messageId}">${reComment.text}</c:if></textarea><br />
					           		<!-- ボタンの追加 -->
				           			<input id="submit" type="submit" value="コメント投稿"><br />
		        				</form>
		        				<br />
		        			</div>
						</div>
					</c:forEach>
					<c:remove var="reComment" scope="session"/>
				</div>
				
				<%-- <c:if test="message.count %5 ==0 "> //			
				if (size%5 == 0) {
//				pageSize = size / 5;
//			} else {
//				pageSize = (size / 5) + 1;
//			}--%>
				<a href="./?page=1" class="page1">1</a>
				<a href="./?page=2" class="page2">2</a>
				<a href="./?page=3" class="page3">3</a>
				<%--</c:if>--%>
				
			</c:if>
		</div>   
		<div class="copyright"> Copyright(c)Ichikawa</div>	
	</body>
</html>