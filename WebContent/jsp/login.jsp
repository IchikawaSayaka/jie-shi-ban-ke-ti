<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン</title>
	<link href="./css/login.css" rel="stylesheet" type="text/css">
</head>
	<body>
		<div class="title"><h1>ログイン</h1></div>
		<div class="main-contents">
            <c:if test="${ not empty loginErrorMessages }">
                <div class="loginErrorMessages">
                        <c:forEach items="${loginErrorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                </div>
                <c:remove var="loginErrorMessages" scope="session"/>
            </c:if>

            <form action="login" method="post">
            <ul>
            	<li class="loginId">
                	<label for="loginId">ログインID</label><br><input name="loginId" value="${reUser.loginId}" id="loginId"/> <br />
                </li>
                <li class="password">
                	<label for="password">パスワード</label><br><input name="password" type="password" id="password"/> <br />
                </li>
                <li class="submit"><br>
               	 <input id="submit" type="submit" value="ログイン" />
                </li>
                <c:remove var="reUser" scope="session"/>
            </ul>
            </form>
        </div>
		<div class="copyright"> Copyright(c)Ichikawa</div>
	</body>
</html>