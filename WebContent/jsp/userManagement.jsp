<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム</title>
		<link href="./css/userManagement.css" rel="stylesheet">
		<script>		
			function checkstop(name) {
			    if (window.confirm(name +'さんのアカウントを停止しますか？')) {
			        return true;
				} else {
					return false;
			    }
			}
		</script>
		<script>
			function checkaction(name) {
			    if (window.confirm(name +'さんのアカウントを再開しますか？')) {
			        return true;
				} else {
					return false;
			    }
			}
		</script>
		
	</head>
	<body>
		<div class="title"><h1>ユーザー管理</h1></div>	
	    
	     <div class="header">
			<div class="left-header">
				<div class="headHome"><a href="./" class="home">ホーム</a></div>
			    <div class="headSignup"><a href="signup" class="signup">ユーザー登録</a></div>
			</div>
			<div class="right-header">
				<div class="profile"><c:out value="ログイン中：${loginUser.name}さん　" />			
				<div class="logout"><a href="logout" class="logout">ログアウト</a></div></div>		
			</div>
		</div>	    
		
		<div class="errorMessages">
        	<c:forEach items="${errorMessages}" var="message">
            	<li><c:out value="${message}" />
            </c:forEach>
            <c:remove var="errorMessages" scope="session" />
		</div>
			
		<div class="main-contents">
			<table>
				<tr>
					<th>支店</th>
					<th>部署・役職</th>
					<th>名前</th>
					<th>ログインID</th>
					<th>活動状態</th>
					<th>ユーザー編集</th>
					
					
				</tr>
				<c:forEach items="${users}" var="user">
					<tr align="center">
						<td>${user.branchName}</td>
						<td>${user.positionName}</td>
						<td>${user.name}</td>
						<td>${user.loginId}</td>
						<c:if test="${user.id == loginUser.id}">
						<td>ログイン中</td>
						</c:if>
						<c:if test="${user.id != loginUser.id}">
							<td><form action="userManagement" method="post">
								<input type="hidden" name="userId" value="${user.id}" id="userId">
								<c:if test="${user.isStopped == 0}">
									<input id="action" type="submit" onClick="return checkstop('${user.name}')" value="活動中">
									<input type="hidden" name="isStopped" value="1" id="isStopped">
								</c:if>
								<c:if test="${user.isStopped == 1}">
									<input id="stop" type="submit" onClick="return checkaction('${user.name}')" value="停止中">
									<input type="hidden" name="isStopped" value="0" id="isStopped">
								</c:if>
								</form>
							</td>
						</c:if>
						<td><a href="edit?userId=${user.id}"><input id="edit" type="submit" value="編集"><input type="hidden" name="userId" value="${user.id}"></a></td>
					</tr>
				</c:forEach>
			</table>  
		</div>
		<div class="copyright"> Copyright(c)Ichikawa</div>			
	</body>
</html>