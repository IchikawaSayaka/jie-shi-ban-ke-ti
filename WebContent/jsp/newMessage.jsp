<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- HTMLの型を宣言 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
		<link href="./css/newMessage.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	 	<div class="title"><h1>新規投稿</h1></div>	
			
	     <div class="header">
			<div class="left-header">
				<div class="headHome"><a href="./" class="home">ホーム</a></div>
			</div>
			<div class="right-header">
				<div class="profile"><c:out value="ログイン中：${loginUser.name}さん　" />
				<div class="logout"><a href="logout" class="logout">ログアウト</a></div></div>		
			</div>
		</div>
						
    	<div class="errorMessages">
	    	<c:if test="${ not empty errorMessages }">
	            	<!-- for文と同じ繰り返し処理 -->
	                <c:forEach items="${errorMessages}" var="message">
	                    <li><c:out value="${message}" />
	                </c:forEach>
	        </c:if>
	        <c:remove var="errorMessages" scope="session" />
        </div>
 		<div class="main-contents">
			<div class="form-area">
        		<form action="newMessage" method="post">
        		<ul>
	          		<li class="title">
	          		タイトル(30文字以下)<br>
	          		<input type="text" size="60" name="title" value="${reMessage.title}" id=title>
	          		</li>
					<li class="categpry">
	          		カテゴリー(10文字以下)<br>
	          		<label for="category"></label> <input name="category" value="${reMessage.category}" id="category" />
               		</li> 
	          		<li class="text">
	          		本文（1000文字以下）<br>
	           		<textarea name="text" cols="80" rows="17" class="tweet-box">${reMessage.text}</textarea>
	           		</li>
	           		<!-- ボタンの追加 -->
	           		<li class="submit"><br>
           			<input id="submit" type="submit" value="新規投稿">
           			<c:remove var="reMessage" scope="session"/>
           		</ul>
        		</form>
        		
        	</div>
		</div>
		<div class="copyright"> Copyright(c)Ichikawa</div>
	</body>
</html>
