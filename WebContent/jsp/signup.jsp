<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー登録</title>
		<link href="./css/signup.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="title"><h1>ユーザー登録</h1></div>
		<!--  name,valueはサーバとのやりとり、id,classはcssで定義 -->

		<div class="header">
			<div class="left-header">
				<div class="headHome"><a href="./" class="home">ホーム</a></div>
			    <div class="headUserManagement"><c:if test="${loginUser.positionId == 1 }"><a href="userManagement" class="userManagement">ユーザー管理</a></c:if></div>
			</div>
			<div class="right-header">
				<div class="profile"><c:out value="ログイン中：${loginUser.name}さん　" />
				<div class="logout"><a href="logout" class="logout">ログアウト</a></div></div>		
			</div>
		</div>
		
		 <div class="errorMessages">
            <!-- for文と同じ繰り返し処理 -->
        	<c:forEach items="${errorMessages}" var="message">
            	<li><c:out value="${message}" />
            </c:forEach>
            <c:remove var="errorMessages" scope="session" />
		</div>
               
		
        <div class="main-contents">
        <!-- SignUpServlrtの@WebServlet(urlPatterns = { "/signup" }に対応 -->
            <form action="signup" method="post">
                <ul>

                <li class="loginId">
                <label for="name">名前(10文字以下)</label><br> 
                <input name="name" value="${reUser.name}" id="name" />
                </li>
                <li class="userId">
                <label for="loginId">ログインID(半角英数6文字以上20文字以下)</label><br> 
                <input name="loginId" value="${reUser.loginId}" id="loginId" />
                </li> 
                <li class="password">
                <label for="password">パスワード(半角英数記号6文字以上20文字以下)</label><br> 
                <input name="password" type="password" id="password" /> 
                </li>
                <li class="passwordCheck">
                <label for="passwordCheck">パスワード確認</label><br> 
                <input name="passwordCheck" type="password" id="passwordCheck" /> 
                </li>
				<li class="branchId">
				<label for="branchId">支店</label><br>
				<select name="branchId">
				<option value="">選択してください</option>
					<c:forEach items="${branches}" var="branch">
						<c:if test="${branch.id == reUser.branchId}">
							<option value="${branch.id}" id="branchId" selected><c:out value="${branch.name}"></c:out></option>
						</c:if>
						<c:if test="${branch.id != reUser.branchId}">
							<option value="${branch.id}" id="branchId"><c:out value="${branch.name}"></c:out></option>
						</c:if>
					</c:forEach>
				</select>
				<br />
				<li class="positionId">
				<label for="positionId">部署・役職</label><br />
				<select name="positionId">
				<option value="">選択してください</option>
					<c:forEach items="${positions}" var="position">
						<c:if test="${position.id == reUser.positionId}">
							<option value="${position.id}" id="positionId" selected><c:out value="${position.name}"></c:out></option>
						</c:if>
						<c:if test="${position.id != null && position.id != reUser.positionId}">
							<option value="${position.id}" id="positionId"><c:out value="${position.name}"></c:out></option>
						</c:if>
					</c:forEach>
				</select>
	        	<%--影響していない--%>
	        	<input type="hidden" name="isStopped" value=1>
                <br />
                <br />
                <li class="submit"><input id="submit" type="submit" value="登録" />
                <c:remove var="reUser" scope="session"/>
                </li>
            </ul>
            </form>
        </div>
	    <div class="copyright">Copyright(c)Ichikawa</div>
	</body>
</html>