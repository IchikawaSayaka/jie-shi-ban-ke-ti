﻿package ichikawa_sayaka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ichikawa_sayaka.beans.Message;
import ichikawa_sayaka.beans.User;
import ichikawa_sayaka.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {
			request.getRequestDispatcher("newMessage.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		Message message = new Message();
		Message reMessage = getReMessage(request);
		
		if (isValid(request, messages) == true) {
			User user = (User) session.getAttribute("loginUser");
    
			message.setUserId(user.getId());
			message.setTitle(request.getParameter("title"));
			message.setCategory(request.getParameter("category"));
			message.setText(request.getParameter("text"));
			
			new MessageService().register(message);
			response.sendRedirect("./");
			
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("reMessage", reMessage);
			request.getRequestDispatcher("newMessage.jsp").forward(request, response);		
		}
	}
	
    private Message getReMessage(HttpServletRequest request)
            throws IOException, ServletException {

    	Message reMessage = new Message();
    	reMessage.setTitle(request.getParameter("title"));
    	reMessage.setCategory(request.getParameter("category"));
    	reMessage.setText(request.getParameter("text"));
        return reMessage;
    }
	
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		if (StringUtils.isBlank(title) == true) {
			messages.add("タイトルを入力してください");
		}	
		if (30 <= title.length()) {
			messages.add("タイトルは30文字以下で入力してください");
		}
		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (10 <= category.length()) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if (StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}
		if (1000 <= text.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}