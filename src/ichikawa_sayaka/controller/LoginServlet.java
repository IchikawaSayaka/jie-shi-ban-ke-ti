﻿package ichikawa_sayaka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ichikawa_sayaka.beans.User;
import ichikawa_sayaka.service.LoginService;

//URLパターン：サーブレットをリクエストする際に呼ばれるもの
@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	//ログイン画面の表示
	//HttpServletRequestはインターフェイスだけどクラスのようなもの
	//GETリクエストはWEBページなどの新しい情報を取得するとき、送信した結果を保存・共有するとき
	//GETリクエストが送信されるのは、アドレスバーにURLを入力、リンクをクリック、method属性がgetのフォームの送信ボタンをクリックしたとき
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {
			request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	//ログインIDとパスワードの取得・送信
	//POSTリクエストはフォームに入力した情報を登録するようなとき、データをアドレスバーに表示したくないとき
	//GETリクエストが送信されるのは、method属性がpostのフォームの送信ボタンをクリックしたとき
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {

		//リクエストスコープに保存するインスタンスを生成し保存
		String loginId = request.getParameter("loginId");	
		String password = request.getParameter("password");
		
		LoginService loginService = new LoginService();
		HttpSession session = request.getSession(); 
		List<String> messages = new ArrayList<String>();
		User user = loginService.login(loginId, password);
		User reUser = getReUser(request);
		
		if (user != null && user.getIsStopped() == 0) {
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
			
		} else {
			messages.add("ログインに失敗しました");
			//セッションスコープはリクエストをまたいでインスタンスを利用可能。
			session.setAttribute("loginErrorMessages", messages);
			//リクエストスコープにインスタンスを保存("属性名",インスタンス)
			session.setAttribute("reUser", reUser);
			response.sendRedirect("login");
//			request.getRequestDispatcher("login").forward(request, response);
		}
	}
	
	//エラーのときの値の保持
    private User getReUser(HttpServletRequest request)
            throws IOException, ServletException {

        User reUser = new User();
        reUser.setLoginId(request.getParameter("loginId"));
        return reUser;
    }
}

