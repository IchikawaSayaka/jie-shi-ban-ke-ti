﻿package ichikawa_sayaka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ichikawa_sayaka.beans.Branch;
import ichikawa_sayaka.beans.Position;
import ichikawa_sayaka.beans.User;
import ichikawa_sayaka.service.UserService;

//登録画面の表示
@WebServlet(urlPatterns = { "/signup" })
	public class SignUpServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {
		
		List<Branch> branches = new UserService().getBranches();
		List<Position> positions = new UserService().getPositions();
		
		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	//入力されたデータの取得・登録
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branches = new UserService().getBranches();
		List<Position> positions = new UserService().getPositions();
		
		List<String> message = new ArrayList<String>();
		HttpSession session = request.getSession();
		User user = new User();
		User reUser = getReUser(request);
		
		if (isValid(request, message) == true) {
			//JavaBeansで定義
			user.setLoginId(request.getParameter("loginId"));
			user.setName(request.getParameter("name"));
			user.setPassword(request.getParameter("password"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setPositionId(Integer.parseInt(request.getParameter("positionId")));
			user.setIsStopped(0);

			new UserService().register(user);
			response.sendRedirect("userManagement");
			
		} else {
			session.setAttribute("errorMessages", message);
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("reUser", reUser);
			request.getRequestDispatcher("signup.jsp").forward(request,response);
		}
	}
	
	//エラーのときの値の保持
    private User getReUser(HttpServletRequest request)
            throws IOException, ServletException {

        User reUser = new User();
        reUser.setLoginId(request.getParameter("loginId"));
        reUser.setName(request.getParameter("name"));
        if(StringUtils.isBlank(request.getParameter("branchId")) != true) {
        	reUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        }
        if(StringUtils.isBlank(request.getParameter("positionId")) != true) {
        	reUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));
        }
        return reUser;
    }
	
	//エラーメッセージの表示
	private boolean isValid(HttpServletRequest request, List<String> login) {
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		String branchId = request.getParameter("branchId");
		String positionId = request.getParameter("positionId");
		
		if (StringUtils.isBlank(loginId) == true) {
			login.add("ログインIDを入力してください");
		} else 	if (!loginId.matches("^[0-9a-zA-Z]{6,20}$")) {
			login.add("ログインIDは半角英数6～20字で入力してください");
		}
		UserService userService = new UserService();
		User account = userService.account(loginId);
		if (StringUtils.isBlank(name) == true) {
			login.add("名前を入力してください");
		} else 	if (10 <= name.length()) {
			login.add("名前は10文字以下で入力してください");
		}
		if (account != null) {
			login.add("ログインIDはすでに使用されています");
		}
		if (StringUtils.isBlank(password) == true) {
			login.add("パスワードを入力してください");
		} else if (!password.matches("^[0-9a-zA-Z\\-\\_\\@\\:\\.\\/\\~]{6,20}$") ) {
			login.add("パスワードは半角英数記号6～20字で入力してください");
		}
		if (StringUtils.isBlank(password) == false && StringUtils.isBlank(passwordCheck) == true) {
			login.add("パスワードを再入力してください");
		}
		if (StringUtils.isBlank(password) == false && StringUtils.isBlank(passwordCheck) == false && !password.equals(passwordCheck)) {
			login.add("パスワードが一致しません");
		}	
		if (StringUtils.isBlank(branchId) == true) {
			login.add("支店を選択してください");
		}
		if (StringUtils.isBlank(positionId) == true) {
			login.add("部署・役職を選択してください");
		}
		if(branchId.equals("1") && positionId.matches("^[3-4]$")) {
			login.add("支店と部署・役職の組み合わせが不正です");
		}
		if(branchId.matches("^[2-4]$") && positionId.matches("^[1-2]$")) {
			login.add("支店と部署・役職の組み合わせが不正です");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (login.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}

