package ichikawa_sayaka.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ichikawa_sayaka.service.CommentService;


@WebServlet(urlPatterns = { "/commentDelete" })
	public class CommentDeleteServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		@Override
		protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

			int commentId = Integer.parseInt(request.getParameter("commentId"));
			new CommentService().getCommentDelete(commentId);
			request.setAttribute("commentId", commentId);
			
			if(request.getParameter("messageId") != null) {
				int messageId = Integer.parseInt(request.getParameter("messageId"));
				new CommentService().getCommentMessageDelete(messageId);
				request.setAttribute("messageId", messageId);
			}
			
			response.sendRedirect("./");
//			request.getRequestDispatcher("/top.jsp").forward(request, response);
		}
}