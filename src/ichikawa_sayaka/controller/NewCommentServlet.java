package ichikawa_sayaka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ichikawa_sayaka.beans.Comment;
import ichikawa_sayaka.beans.User;
import ichikawa_sayaka.service.CommentService;


@WebServlet(urlPatterns = { "/newComment" })
	public class NewCommentServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		//コメントの表示
		@Override
		protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

			List<Comment> comments = new CommentService().getComments();

			request.setAttribute("Comments", comments);
			request.getRequestDispatcher("/top.jsp").forward(request, response);
		}

		//コメントの取得
		@Override
		protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

			List<String> comments = new ArrayList<String>();
			HttpSession session = request.getSession();
			Comment comment = new Comment();
			Comment reComment = getReComment(request);
			
			if (isValid(request, comments) == true) {
				User user = (User) session.getAttribute("loginUser");
	
				//jspとひもづけ
				comment.setUserId(user.getId());
				comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
				comment.setText(request.getParameter("text"));

				new CommentService().register(comment);
				
				response.sendRedirect("./");
			} else {
				session.setAttribute("errorMessages", comments);
				session.setAttribute("reComment", reComment);
				response.sendRedirect("./");
			}
		}
		
		//コメントエラーの値の保持
		private Comment getReComment(HttpServletRequest request)
				throws IOException, ServletException {
		
			Comment reComment = new Comment();
			reComment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
			reComment.setText(request.getParameter("text"));
			return reComment;
		}
		
		private boolean isValid(HttpServletRequest request, List<String> comments) {
			String text = request.getParameter("text");

			if (StringUtils.isBlank(text) == true) {
				comments.add("コメントを入力してください");
			}
			if (500 <= text.length()) {
				comments.add("コメントは500文字以下で入力してください");
			}
			if (comments.size() == 0) {
				return true;
			} else {
				return false;
			}
		}
}