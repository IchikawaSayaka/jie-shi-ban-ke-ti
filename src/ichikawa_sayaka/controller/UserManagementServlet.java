package ichikawa_sayaka.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ichikawa_sayaka.beans.User;
import ichikawa_sayaka.service.UserService;

//URLパターン：サーブレットをリクエストする際に呼ばれるもの
@WebServlet(urlPatterns = { "/userManagement" })
public class UserManagementServlet extends HttpServlet {	
	private static final long serialVersionUID = 1L;

	@Override
	//1　servletパスを指定することでservletとjspファイルをつなげる　
	//ユーザー情報の一覧を取得して表示する
	protected void doGet(HttpServletRequest request,
	HttpServletResponse response) throws IOException, ServletException {
		
		List<User> users = new UserService().getUsers();

		request.setAttribute("users", users);
		
		//1 servletとjspファイルをつなげる
		request.getRequestDispatcher("/userManagement.jsp").forward(request, response);
	}
	
	@Override
	//活動状態の変更
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {

			User action = new User();
			action.setId(Integer.parseInt(request.getParameter("userId")));
			action.setIsStopped(Integer.parseInt(request.getParameter("isStopped")));
		
			new UserService().action(action);

			request.setAttribute("action", action);
			response.sendRedirect("userManagement");
	}
}
