package ichikawa_sayaka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ichikawa_sayaka.beans.Branch;
import ichikawa_sayaka.beans.Position;
import ichikawa_sayaka.beans.User;
import ichikawa_sayaka.service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	//編集するユーザーデータの取得
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {
		
		List<Branch> branches = new UserService().getBranches();
		List<Position> positions = new UserService().getPositions();
		List<String> edits = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (request.getParameter("userId").matches("^[0-9]+$")) {
			//正規表現の確認
			int id = Integer.parseInt(request.getParameter("userId"));

			User editUser = new UserService().getUsers(id);
			if (editUser != null) {

				request.setAttribute("branches", branches);
				request.setAttribute("positions", positions);
				request.setAttribute("editUser", editUser);
				
				//forwardできてないと画面がまっしろになる
				request.getRequestDispatcher("edit.jsp").forward(request, response);
				//returnでif文の外にかえる
				return;
			}
		}
		//if文が実行されなかったとき、必ずとおる
		edits.add("不正なパラメーターです");
		session.setAttribute("errorMessages", edits);
		response.sendRedirect("./userManagement");
	}

	@Override
	//ユーザー編集のデータをjspからとってきて上書きする
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {
		
		List<Branch> branches = new UserService().getBranches();
		List<Position> positions = new UserService().getPositions();
		
		List<String> edits = new ArrayList<String>();
		HttpSession session = request.getSession();
		User edit = new User();
		User editUser = getReEditUser(request);

		if (isValid(request, edits) == true) {
			//JavaBeansで定義 name""
			edit.setId(Integer.parseInt(request.getParameter("userId")));
			edit.setLoginId(request.getParameter("loginId"));
			edit.setName(request.getParameter("name"));
			
			if (StringUtils.isEmpty(request.getParameter("password")) != true) {
				edit.setPassword(request.getParameter("password"));
			}
			
			edit.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			edit.setPositionId(Integer.parseInt(request.getParameter("positionId")));

			new UserService().editer(edit);
			response.sendRedirect("userManagement");
		} else {
			session.setAttribute("errorMessages", edits);
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
		}
	}
	
	//エラーのときの値の保持
    private User getReEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("userId")));
        editUser.setLoginId(request.getParameter("loginId"));
        editUser.setName(request.getParameter("name"));
        editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));
        return editUser;
    }
	
	//エラーメッセージの表示
	private boolean isValid(HttpServletRequest request, List<String> update) {
		int id = Integer.parseInt(request.getParameter("userId"));
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		String branchId = request.getParameter("branchId");
		String positionId = request.getParameter("positionId");
		
		User editUser = new UserService().getUsers(id);
		String editLoginId = editUser.getLoginId();

		if (StringUtils.isBlank(name) == true) {
			update.add("名前を入力してください");
		} else 	if (10 <= name.length()) {
			update.add("名前は10文字以下で入力してください");
		}
		if (StringUtils.isBlank(loginId) == true) {
			update.add("ログインIDを入力してください");
		} else	if (!loginId.matches("^[0-9a-zA-Z]{6,10}$")) {
			update.add("ログインIDは半角英数6～20字で入力してください");
		}
		if (!loginId.equals(editLoginId)) {
			UserService userService = new UserService();
			User account = userService.account(loginId);
			if (account != null) {
				update.add("ログインIDはすでに使用されています");
			}	
		}
		//パスワードは変更しなくてもOK
		if (StringUtils.isBlank(password) == false && !password.matches("^[0-9a-zA-Z\\-\\_\\@\\:\\.\\/\\~]{6,20}$")) {
			update.add("パスワードは半角英数記号6～20字で入力してください");
		}
		if (StringUtils.isBlank(password) == false && StringUtils.isBlank(passwordCheck) == true) {
			update.add("パスワードを再入力してください");
		}
		if (StringUtils.isBlank(password) == false && StringUtils.isBlank(passwordCheck) == false && !password.equals(passwordCheck)) {
			update.add("パスワードが一致しません");
		}
		if(branchId.equals("1") && positionId.matches("^[3-4]$")) {
			update.add("支店と部署・役職の組み合わせが不正です");
		}
		if(branchId.matches("^[2-4]$") && positionId.matches("^[1-2]$")) {
			update.add("支店と部署・役職の組み合わせが不正です");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (update.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}


