package ichikawa_sayaka.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ichikawa_sayaka.service.CommentService;
import ichikawa_sayaka.service.MessageService;


@WebServlet(urlPatterns = { "/messageDelete" })
	public class MessageDeleteServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		@Override
		protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

			int messageId = Integer.parseInt(request.getParameter("messageId"));
			
			new MessageService().getMessages(messageId);
			new CommentService().getCommentMessages(messageId);
			
			request.setAttribute("messageId", messageId);
			
			response.sendRedirect("./");
//			request.getRequestDispatcher("/top.jsp").forward(request, response);
		}
}