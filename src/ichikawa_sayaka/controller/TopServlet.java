package ichikawa_sayaka.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ichikawa_sayaka.beans.Comment;
import ichikawa_sayaka.beans.UserMessage;
import ichikawa_sayaka.service.CommentService;
import ichikawa_sayaka.service.MessageService;

//URLパターン：サーブレットをリクエストする際に呼ばれるもの
@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {	
	private static final long serialVersionUID = 1L;

	//サーブレットクラスからJSPファイルにフォワードして、出力処理をJSPに任せる
	//HttpServletRequestはインターフェイスだけどクラスのようなもの
	//GETリクエストはWEBページなどの新しい情報を取得するとき、送信した結果を保存・共有するとき	
	//POSTリクエストはjspで取得したデータを送るとき
	@Override
	protected void doGet(HttpServletRequest request,
	HttpServletResponse response) throws IOException, ServletException {
		
		HttpSession session = request.getSession();

		String firstDate;
		String lastDate;
		String searchCategory = request.getParameter("searchCategory");
		int page;
		
		//はじまりがnullのときは、かなり前の日にちをいれる
		if(request.getParameter("firstDate") == null || request.getParameter("firstDate").isEmpty() == true) {
			firstDate = "2018/05/01";
		} else {
			firstDate = request.getParameter("firstDate");
		}
		
		//おわりがnullのときは、今日の日にちをいれる
		if(request.getParameter("lastDate") == null || request.getParameter("lastDate").isEmpty() == true) {
			Date date = new Date();
			lastDate = new String(new SimpleDateFormat("yyyy/MM/dd").format(date));
		} else {
			lastDate = request.getParameter("lastDate");
		}
		
		if(request.getParameter("page") == null) {
			page = Integer.parseInt("0");
		} else {
			page = (Integer.parseInt(request.getParameter("page")) - 1) *5;
		}
		
		System.out.println(page);
		session.setAttribute("firstDate", firstDate);
		session.setAttribute("lastDate", lastDate);
		session.setAttribute("searchCategory", searchCategory);
		session.setAttribute("page", page);

		//messageの表示
		List<UserMessage> messages;
		try {
			messages = new MessageService().getMessage(searchCategory, firstDate, lastDate, page);
			request.setAttribute("messages", messages);
			
//			int size = messages.size();
//			int pageSize;
//			if (size%5 == 0) {
//				pageSize = size / 5;
//			} else {
//				pageSize = (size / 5) + 1;
//			}
//
//			System.out.println(size);
//			System.out.println(pageSize);
//			request.setAttribute("size", pageSize);
			
			//commentの表示
			List<Comment> comments = new CommentService().getComments();
			request.setAttribute("comments", comments);
			
			//検索機能の格納
			request.setAttribute("firstDate", firstDate);
			request.setAttribute("lastDate", lastDate);
			request.setAttribute("searchCategory", searchCategory);

			
			request.getRequestDispatcher("/top.jsp").forward(request, response);
		
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}
}

