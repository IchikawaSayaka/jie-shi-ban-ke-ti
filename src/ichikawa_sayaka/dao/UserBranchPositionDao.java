package ichikawa_sayaka.dao;

import static ichikawa_sayaka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ichikawa_sayaka.beans.User;
import ichikawa_sayaka.exception.SQLRuntimeException;

//UserDaoとMessageDaoを結合させる①
public class UserBranchPositionDao {
	
	public List<User> getUsers(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//appendで文字列の結合
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("branches.name as branch, ");
			sql.append("users.position_id as position_id, ");
			sql.append("positions.name as position, ");
			sql.append("users.is_Stopped as is_Stopped ");
			
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("ORDER BY branch_id, position_id, name DESC ");

			ps = connection.prepareStatement(sql.toString());

			//resultsetでデータをもってきて、つかいやすいListの形に変換する
			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//UserDaoとMessageDaoを結合させる②
	private List<User> toUserList(ResultSet rs)
	throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				//DBのColum名
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String branchName = rs.getString("branch");
				String positionName = rs.getString("position");
				int isStopped = rs.getInt("is_stopped");
				
				User users = new User();
				users.setId(id);
				users.setLoginId(loginId);
				users.setName(name);
				users.setBranchName(branchName);
				users.setPositionName(positionName);
				users.setIsStopped(isStopped);

				//userリストに結果を返す
				ret.add(users);
			} 
		return ret;
		} finally {
			close(rs);
		}
	}
}
