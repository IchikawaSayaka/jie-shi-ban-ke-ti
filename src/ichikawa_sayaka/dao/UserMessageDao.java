package ichikawa_sayaka.dao;

import static ichikawa_sayaka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ichikawa_sayaka.beans.UserMessage;
import ichikawa_sayaka.exception.SQLRuntimeException;

//Messageの表示①
//UserDaoとMessageDaoを結合させる
public class UserMessageDao {
	//検索機能の追加
	public List<UserMessage> getMessages(Connection connection, String searchCategory, String firstDate, String lastDate, int page) throws ParseException {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("users.branch_id as branchId, ");
			sql.append("users.position_id as positionId, ");
			sql.append("messages.title as title, ");
			sql.append("messages.category as category, ");
			sql.append("messages.text as text, ");
			sql.append("messages.created_date as created_date ");
//			sql.append("count(id) as count ");
			
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON user_id = users.id "); 
			
			sql.append("Where created_date between ? AND ? ");
			
			//カテゴリー検索がnullでないとき
			if(searchCategory != null && StringUtils.isBlank(searchCategory) != true) {
				sql.append("AND category like ? ");
			}
			
			sql.append("ORDER BY created_date DESC ");
			sql.append("LIMIT 5 OFFSET ? ");
			
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, firstDate + " 00:00:00");
			ps.setString(2, lastDate + " 23:59:59");

			if(searchCategory != null && StringUtils.isBlank(searchCategory) != true) {
				//部分一致で検索
				ps.setString(3, "%" + searchCategory + "%");
				ps.setInt(4, page);
			}
			
			if(searchCategory == null || StringUtils.isBlank(searchCategory) == true) {
				ps.setInt(3, page);
			}
			
			System.out.println(ps.toString());
			
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//Messageの表示②
	private List<UserMessage> toMessageList(ResultSet rs)
	throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				//DBのColum名
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int branchId = rs.getInt("branchId");
				int positionId = rs.getInt("positionId");
				String title = rs.getString("title");
				String category = rs.getString("category");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
				
				UserMessage messages = new UserMessage();
				messages.setId(id);
				messages.setName(name);
				messages.setBranchId(branchId);
				messages.setPositionId(positionId);
				messages.setTitle(title);
				messages.setCategory(category);
				messages.setText(text);
				messages.setCreatedDate(createdDate);
				
				ret.add(messages);
			} 
		return ret;
		} finally {
			close(rs);
		}
	}
//	
//	public User getUser(Connection connection) {
//
//		PreparedStatement ps = null;
//		try {
//			String sql = "SELECT count(*) FROM messages";
//
//			ResultSet rs = ps.executeQuery();
//			int count = getInt(rs);
//
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		} finally {
//			close(ps);
//		}
//	}
}
