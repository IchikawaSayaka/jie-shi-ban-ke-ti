package ichikawa_sayaka.dao;

import static ichikawa_sayaka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ichikawa_sayaka.beans.Comment;
import ichikawa_sayaka.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("user_id");
			sql.append(", message_id");
			sql.append(", text");
			sql.append(", created_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // user_id
			sql.append(", ?"); // message_id
			sql.append(", ?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getUserId());
			ps.setInt(2, comment.getMessageId());
			ps.setString(3, comment.getText());
			System.out.println(ps.toString());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	public List<Comment> getComments(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//commentsテーブルのuser_idカラムをuser_idとおく
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.message_id as message_id, ");
			sql.append("users.name as name, ");
			sql.append("users.branch_id as branchId, ");
			sql.append("users.position_id as positionId, ");
			sql.append("comments.text as text, ");
			sql.append("comments.created_date as created_date ");
			
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");

			sql.append("ORDER BY created_date");

			ps = connection.prepareStatement(sql.toString());

			//SELECT文
			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Comment> toCommentList(ResultSet rs)
	throws SQLException {

		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				//DBのColum名
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int branchId = rs.getInt("branchId");
				int positionId = rs.getInt("positionId");
				int messageId = rs.getInt("message_id");
				String name = rs.getString("name");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
	
				Comment comments = new Comment();
				comments.setId(id);
				comments.setUserId(userId);
				comments.setMessageId(messageId);
				comments.setBranchId(branchId);
				comments.setPositionId(positionId);
				comments.setName(name);
				comments.setText(text);
				comments.setCreatedDate(createdDate);
	
				ret.add(comments);
			} 
		return ret;
		} finally {
			close(rs);
		}
	}

	public void delete(Connection connection, Integer commentId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//テンプレート
			sql.append("DELETE FROM  comments");
			sql.append(" WHERE id = ?");
			
			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, commentId);

			//実行処理
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	public void deleteMessage(Connection connection, Integer messageId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//テンプレート
			sql.append("DELETE FROM  comments");
			sql.append(" WHERE message_id = ?");
			
			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, messageId);

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}