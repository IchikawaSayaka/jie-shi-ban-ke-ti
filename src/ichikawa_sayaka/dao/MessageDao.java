package ichikawa_sayaka.dao;

import static ichikawa_sayaka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ichikawa_sayaka.beans.Message;
import ichikawa_sayaka.exception.SQLRuntimeException;

public class MessageDao {

	//Messageの登録
	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("user_id");
			sql.append(", title");
			sql.append(", category");
			sql.append(", text");
			sql.append(", created_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // user_id
			sql.append(", ?"); // title
			sql.append(", ?"); // category
			sql.append(", ?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getTitle());
			ps.setString(3, message.getCategory());
			ps.setString(4, message.getText());
			System.out.println(ps.toString());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//Messageの削除
	public void delete(Connection connection, Integer messageId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//テンプレート
			sql.append("DELETE FROM  messages");
			sql.append(" WHERE id = ?");
			
			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, messageId);

			//実行処理
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}