﻿package ichikawa_sayaka.dao;

import static ichikawa_sayaka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ichikawa_sayaka.beans.User;
import ichikawa_sayaka.exception.SQLRuntimeException;


public class UserDao {
	
	//insertだけを実装したDaoクラス
	//ユーザーの新規登録
	public void insert(Connection connection, User user) {
	
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//テンプレート
			sql.append("INSERT INTO users ( ");
			sql.append("login_id ");
			sql.append(", name");
			sql.append(", password");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", is_stopped");
			sql.append(") VALUES (");
			sql.append(" ?"); // login_id
			sql.append(", ?"); // name
			sql.append(", ?"); // password
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // position_id
			sql.append(", ?"); // is_stopped
			sql.append(")");

			//実行の予約
			ps = connection.prepareStatement(sql.toString());

			//1つめの？にはこの値・・・
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getPositionId());
			ps.setInt(6, user.getIsStopped());

			//実行されたSQL文がコンソール上に表示される→MySQLでエラーを表示してもらう
			System.out.println(ps.toString());

			//実行処理
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	//ログインIDの重複の確認
	public User getAccount(Connection connection, String loginId) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_Id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	//ログインIDとパスワードの取得し、合致するユーザーの検索①
	public User getUser(Connection connection, String loginId,	String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_Id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (3 <= userList.size()) {
				throw new IllegalStateException("3 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ログインIDとパスワードの取得し、合致するユーザーの検索、表示
	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_Id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int branchId = rs.getInt("branch_Id");
				int positionId = rs.getInt("position_Id");
				int isStopped = rs.getInt("is_stopped");

			User user = new User();
			user.setId(id);
			user.setLoginId(loginId);
			user.setName(name);
			user.setPassword(password);
			user.setBranchId(branchId);
			user.setPositionId(positionId);
			user.setIsStopped(isStopped);

			ret.add(user);
		}
			return ret;
		} finally {
			close(rs);
		}
	}
	
	//メッセージの表示①
	public List<User> getMessages(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.title as title, ");
			sql.append("messages.category as category, ");
			sql.append("messages.text as text, ");
			sql.append("messages.created_date as created_date ");
			
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id "); //?
			sql.append("ORDER BY created_date DESC ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	//メッセージの表示②
	private List<User> toMessageList(ResultSet rs)
	throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				//DBのColum名
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String title = rs.getString("title");
				String category = rs.getString("category");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
	
				User messages = new User();
				messages.setId(id);
				messages.setName(name);
				messages.setTitle(title);
				messages.setCategory(category);
				messages.setText(text);
				messages.setCreatedDate(createdDate);
	
				//messageリストに結果を返す
				ret.add(messages);
			} 
		return ret;
		} finally {
			close(rs);
		}
	}
	
	//ユーザー編集画面にuserIdが合致するデータを表示する
	public User getEditUser(Connection connection, int userId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("users.password as password, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("users.position_id as position_id, ");
			sql.append("users.is_Stopped as is_Stopped ");
			
			sql.append("FROM users ");
			sql.append("WHERE id = ?; ");
			
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			
			//リストから1件分だけデータをもってくる
			if(userList != null && userList.size() != 0){
				return userList.get(0);
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ユーザー編集のデータの上書き
	public void update(Connection connection, User edit) {
	
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("login_id = ?");
			sql.append(", name = ?");
			
			if (StringUtils.isEmpty(edit.getPassword()) != true) {
				sql.append(", password = ?");
			}
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, edit.getLoginId());
			ps.setString(2, edit.getName());
			
			if (StringUtils.isEmpty(edit.getPassword()) != true) {
				ps.setString(3, edit.getPassword());
				ps.setInt(4, edit.getBranchId());
				ps.setInt(5, edit.getPositionId());
				ps.setInt(6, edit.getId());
			} else {
				ps.setInt(3, edit.getBranchId());
				ps.setInt(4, edit.getPositionId());
				ps.setInt(5, edit.getId());
			}
			
			System.out.println(ps.toString());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	//活動・停止の変更
	public void isStopped(Connection connection, User action) {
		
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("is_stopped = ?");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, action.getIsStopped());
			ps.setInt(2, action.getId());

			System.out.println(ps.toString());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}