package ichikawa_sayaka.dao;

import static ichikawa_sayaka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ichikawa_sayaka.beans.Branch;
import ichikawa_sayaka.exception.SQLRuntimeException;

public class BranchDao {
	
	//insertだけを実装したDaoクラス
	public void insert(Connection connection, Branch branch) {
	
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//テンプレート
			//INSERT INTOでテーブルにレコードを追加
			sql.append("INSERT INTO branches ( ");
			sql.append("id");
			sql.append(", name");
			sql.append(") VALUES (");
			sql.append(" ?"); // id
			sql.append(", ?"); // name
			sql.append(")");

			//実行の予約
			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, branch.getId());
			ps.setString(2, branch.getName());

			//実行されたSQL文がコンソール上に表示される→MySQLでエラーを表示してもらう
			System.out.println(ps.toString());

			//実行処理
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	public List<Branch> getBranches(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//appendで文字列の結合
			sql.append("SELECT ");
			sql.append("branches.id as id, ");
			sql.append("branches.name as branch ");
			
			sql.append("FROM branches ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranchList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	private List<Branch> toBranchList(ResultSet rs)
	throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("branch");
				
				Branch branches = new Branch();
				branches.setId(id);
				branches.setName(name);

				ret.add(branches);
			} 
		return ret;
		} finally {
			close(rs);
		}
	}
}