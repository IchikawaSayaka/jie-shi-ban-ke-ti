package ichikawa_sayaka.dao;

import static ichikawa_sayaka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ichikawa_sayaka.beans.Position;
import ichikawa_sayaka.exception.SQLRuntimeException;


public class PositionDao {
	
	//insertだけを実装したDaoクラス
	public void insert(Connection connection, Position positon) {
	
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//テンプレート
			//INSERT INTOでテーブルにレコードを追加
			sql.append("INSERT INTO positons ( ");
			sql.append("id");
			sql.append(", name");
			sql.append(") VALUES (");
			sql.append(" ?"); // id
			sql.append(", ?"); // name
			sql.append(")");

			//実行の予約
			ps = connection.prepareStatement(sql.toString());

			//1つめの？にはこの値・・・
			ps.setInt(1, positon.getId());
			ps.setString(2, positon.getName());

			//実行されたSQL文がコンソール上に表示される→MySQLでエラーを表示してもらう
			System.out.println(ps.toString());

			//実行処理
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	public List<Position> getPositions(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//appendで文字列の結合
			sql.append("SELECT ");
			sql.append("positions.id as id, ");
			sql.append("positions.name as position ");
			
			sql.append("FROM positions ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Position> ret = toPositionList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	private List<Position> toPositionList(ResultSet rs)
	throws SQLException {

		List<Position> ret = new ArrayList<Position>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("position");
				
				Position positions = new Position();
				positions.setId(id);
				positions.setName(name);

				ret.add(positions);
			} 
		return ret;
		} finally {
			close(rs);
		}
	}
}
