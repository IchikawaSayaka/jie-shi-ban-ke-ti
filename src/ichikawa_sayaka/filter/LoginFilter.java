package ichikawa_sayaka.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ichikawa_sayaka.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		// login の画面でループにならないように
		// 文字列の比較なのでequals
		if ((req.getServletPath().equals("/login")) || (req.getServletPath().substring(req.getServletPath().length()-3).equals("css"))) {
			chain.doFilter(request, response);
			return;
		}
		
		HttpSession session = req.getSession();
		if ((User) session.getAttribute("loginUser") == null) {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください");
			session.setAttribute("loginErrorMessages", messages);
			res.sendRedirect("login");
		}else{
			chain.doFilter(request, response);
		}

	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ

	}
}
