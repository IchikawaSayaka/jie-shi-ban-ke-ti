package ichikawa_sayaka.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ichikawa_sayaka.beans.User;

@WebFilter(urlPatterns = {"/userManagement","/edit","/signup"})
public class ManagementFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
    	FilterChain chain) throws IOException, ServletException {
    	HttpServletRequest req = (HttpServletRequest)request;
    	HttpServletResponse res = (HttpServletResponse)response;
    	
    	HttpSession session = req.getSession();
    	User user = (User) session.getAttribute("loginUser");
    	int positionId = user.getPositionId(); 
		
		if (positionId != 1) {
			List<String> messages = new ArrayList<String>();
			messages.add("ユーザー編集の権限がありません");
			session.setAttribute("managementErrorMessages", messages);
			res.sendRedirect("./");
	        return;
		}
		
		chain.doFilter(request, response);
    }

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ
	}    
}
