package ichikawa_sayaka.service;

import static ichikawa_sayaka.utils.CloseableUtil.*;
import static ichikawa_sayaka.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ichikawa_sayaka.beans.Comment;
import ichikawa_sayaka.dao.CommentDao;

public class CommentService {
	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public List<Comment> getComments() {
		
		Connection connection  = null;
		try { 
			connection = getConnection();
		
			CommentDao commentDao = new CommentDao();
			List<Comment> ret = commentDao.getComments(connection);
	
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void getCommentDelete(Integer commentId) {
		
		Connection connection  = null;
		try { 
			connection = getConnection();
		
			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection, commentId);
	
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public void getCommentMessageDelete(Integer messageId) {
		
		Connection connection  = null;
		try { 
			connection = getConnection();
		
			CommentDao commentDao = new CommentDao();
			commentDao.deleteMessage(connection, messageId);
	
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public void getCommentMessages(Integer messageId) {
		
		Connection connection  = null;
		try { 
			connection = getConnection();
		
			CommentDao commentDao = new CommentDao();
			commentDao.deleteMessage(connection, messageId);
	
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
}
