package ichikawa_sayaka.service;

import static ichikawa_sayaka.utils.CloseableUtil.*;
import static ichikawa_sayaka.utils.DBUtil.*;

import java.sql.Connection;
import java.text.ParseException;
import java.util.List;

import ichikawa_sayaka.beans.Message;
import ichikawa_sayaka.beans.UserMessage;
import ichikawa_sayaka.dao.MessageDao;
import ichikawa_sayaka.dao.UserMessageDao;


public class MessageService {
	
	//Messageの登録
	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

            new MessageDao().insert(connection, message);
            
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//Messageの表示
	//UserMessageDaoでつくったmessageリストの結果を返す
	//getMessageでTopServletにかえす
	public List<UserMessage> getMessage(String searchCategory, String firstDate, String lastDate, int page) throws ParseException {
	
		Connection connection  = null;
		try { 
			connection = getConnection();
		
			UserMessageDao userMessageDao = new UserMessageDao();
			//getMessagesでUserMessageDaoからデータをとってくる
			List<UserMessage> ret = userMessageDao.getMessages(connection, searchCategory, firstDate, lastDate, page);
	
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	//Messageの削除登録
	public void getMessages(int messageId) {
		
		Connection connection  = null;
		try { 
			connection = getConnection();
		
			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, messageId);
	
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}

