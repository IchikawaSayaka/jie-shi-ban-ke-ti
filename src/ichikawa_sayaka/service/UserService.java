﻿package ichikawa_sayaka.service;

import static ichikawa_sayaka.utils.CloseableUtil.*;
import static ichikawa_sayaka.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ichikawa_sayaka.beans.Branch;
import ichikawa_sayaka.beans.Position;
import ichikawa_sayaka.beans.User;
import ichikawa_sayaka.dao.BranchDao;
import ichikawa_sayaka.dao.PositionDao;
import ichikawa_sayaka.dao.UserBranchPositionDao;
import ichikawa_sayaka.dao.UserDao;
import ichikawa_sayaka.utils.CipherUtil;

public class UserService {
	//ユーザーの新規登録
	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			//insert データの追加
			userDao.insert(connection, user);

			UserDao userDaoUpdate = new UserDao();
			userDaoUpdate.update(connection, user);
			
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	//ログインIDの重複の確認
	public User account(String loginId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getAccount(connection, loginId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public List<Branch> getBranches() {
		
		Connection connection  = null;
		try { 
			connection = getConnection();
		
			BranchDao branchDao = new BranchDao();
			List<Branch> ret = branchDao.getBranches(connection);
	
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public List<Position> getPositions() {
		
		Connection connection  = null;
		try { 
			connection = getConnection();
		
			PositionDao positionDao = new PositionDao();
			List<Position> ret = positionDao.getPositions(connection);
	
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	//ユーザー管理画面にユーザー情報の一覧を表示する
	public List<User> getUsers() {
		
		Connection connection  = null;
		try { 
			connection = getConnection();
		
			UserBranchPositionDao userBranchPositionDao = new UserBranchPositionDao();
			List<User> ret = userBranchPositionDao.getUsers(connection);
	
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	//ユーザー編集画面にuserIdが合致するデータを表示する
	public User getUsers(int userId) {
		
		Connection connection  = null;
		User editUser = new User();
		try { 
			connection = getConnection();
		
			UserDao userDao = new UserDao();
			editUser = userDao.getEditUser(connection, userId);
	
			commit(connection);
			
			return editUser;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	//ユーザー編集のデータの上書き
	public void editer(User edit) {

		Connection connection = null;
		try {
			connection = getConnection();

			if (StringUtils.isEmpty(edit.getPassword()) != true) {
				String encPassword = CipherUtil.encrypt(edit.getPassword());
				edit.setPassword(encPassword);
			}
			
			UserDao userDao = new UserDao();
			userDao.update(connection, edit);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	//活動・停止の変更
	public void action(User userId) {

		Connection connection = null;
		try {
			connection = getConnection();
			
			UserDao userActionDao = new UserDao();
			userActionDao.isStopped(connection, userId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
